<!DOCTYPE html>
<html lang="en" class="no-js">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <title>News Agency / Home</title>
    <meta content="" name="description"/>
    <meta content="" name="keywords"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta content="telephone=no" name="format-detection"/>
    <meta name="HandheldFriendly" content="true"/>
    <link rel="stylesheet" href="assets/css/master.css"/>
    <!-- SWITCHER-->
    <link href="assets/plugins/switcher/css/switcher.css" rel="stylesheet" id="switcher-css" media="all"/>
    <link href="assets/plugins/switcher/css/color1.css" rel="alternate stylesheet" title="color1" media="all"/>
    <link href="assets/plugins/switcher/css/color2.css" rel="alternate stylesheet" title="color2" media="all"/>
    <link href="assets/plugins/switcher/css/color3.css" rel="alternate stylesheet" title="color3" media="all"/>
    <link href="assets/plugins/switcher/css/color4.css" rel="alternate stylesheet" title="color4" media="all"/>
    <link href="assets/plugins/switcher/css/color5.css" rel="alternate stylesheet" title="color5" media="all"/>
    <link rel="icon" type="image/x-icon" href="favicon.ico"/>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
  </head>
  <body>
    <!-- Loader-->
    <div id="page-preloader"><span class="spinner"></span></div>
    <!-- Loader end-->
    <div data-header="sticky" data-header-top="200" class="layout-theme animated-css">
      
      <div class="cd-main">
        <div class="wrap-content cd-section cd-selected">
          <header class="header">
            <div class="top-header">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="top-header__link bg-primary">
                      <div class="top-header__wrap-link">top headlines<i class="icon fa fa-caret-right"></i></div>
                    </div>
                    <div class="top-header__ticker">
                      <ul id="myUl">
                        <li class="news-item"><a href="news_details-1.html" class="news-item__link">Uber offers $29m 'safe ride' settlement</a><i class="icon fa fa-adjust"></i></li>
                        <li class="news-item"><a href="news_details-1.html" class="news-item__link">The robot that camouflages itself</a><i class="icon fa fa-adjust"></i></li>
                        <li class="news-item"><a href="news_details-1.html" class="news-item__link">Luxury cars at the Detroit auto show</a><i class="icon fa fa-adjust"></i></li>
                        <li class="news-item"><a href="news_details-1.html" class="news-item__link">Uber offers $29m 'safe ride' settlement</a><i class="icon fa fa-adjust"></i></li>
                        <li class="news-item"><a href="news_details-1.html" class="news-item__link">The robot that camouflages itself</a><i class="icon fa fa-adjust"></i></li>
                        <li class="news-item"><a href="news_details-1.html" class="news-item__link">Luxury cars at the Detroit auto show</a><i class="icon fa fa-adjust"></i></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="header-main">
              <div class="container">
                <div class="row">
                  <div class="col-md-4">
                    <ul class="social-links list-inline">
                      <li><a href="twitter.com" class="social-links__item_link"><i class="icon fa fa-twitter"></i></a></li>
                      <li><a href="facebook.com" class="social-links__item_link"><i class="icon fa fa-facebook"></i></a></li>
                      <li><a href="instagram.com" class="social-links__item_link"><i class="icon fa fa-instagram"></i></a></li>
                      <li><a href="linkedin.com" class="social-links__item_link"><i class="icon fa fa-linkedin"></i></a></li>
                      <li><a href="pinterest.com" class="social-links__item_link"><i class="icon fa fa-pinterest-p"></i></a></li>
                      <li><a href="youtube.com" class="social-links__item_link"><i class="icon fa fa-youtube-play"></i></a></li>
                      <li><a href="rss.com" class="social-links__item_link"><i class="icon fa fa-rss"></i></a></li>
                    </ul>
                  </div>
                  <div class="col-md-4"><a href="home.html" class="logo"><img src="assets/media/general/logo_mod-a.png" alt="Logo" class="logo__img img-responsive center-block"/></a></div>
                  <div class="col-md-4">
                    <div class="header-main__links"><a href="home.html" class="header-main__links-item">Sign In</a> or<a href="home.html" class="header-main__links-item"> Register</a><a href="#fakelink" class="search-open"><i class="icon pe-7s-search"></i></a><a href="#cd-nav" class="trigger cd-nav-trigger"></a></div>
                  </div>
                </div>
              </div>
            </div>
          </header>
          <div class="wrap-nav">
            <nav class="navbar yamm">
              <div id="navbar-collapse-1" class="navbar-collapse collapse">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <ul class="nav navbar-nav">
                        <li><a href="home.html">All pages</a>

                            <ul class="dropdown-menu">


                            <li><a href="home.html">HOMEPAGE </a>
                              <ul class="dropdown-menu">
                               <li><a href="home.html">HOMEPAGE 1</a></li>
                              <li><a href="home-2.html">HOMEPAGE 2</a></li>
                              <li><a href="home-3.html">HOMEPAGE 3</a></li>
                             </ul>
                            </li>


                                <li><a href="category.html">NEWS </a>
                              <ul class="dropdown-menu">
                                   <li><a href="category.html">Blog page</a></li>
                               <li><a href="news_details-1.html">news details 1</a></li>
                              <li><a href="news_details-2.html">news details 2</a></li>
                              <li><a href="news_details-3.html">news details 3</a></li>
                             </ul>
                            </li>


                                <li><a href="about.html">pages </a>
                              <ul class="dropdown-menu">
                               <li><a href="about.html">ABOUT</a></li>
                              <li><a href="contacts.html">CONTACT</a></li>
                              <li><a href="typography.html">typography</a></li>
                             </ul>
                            </li>



                          </ul>
                        </li>
                        <li class="yamm-fw"><a href="home.html">Health</a></li>
                        <li class="yamm-fw"><a href="home.html">technology</a></li>
                        <li class="yamm-fw"><a href="home.html">politics</a></li>
                        <li class="yamm-fw"><a href="home.html">entertainment</a></li>
                        <li class="yamm-fw "><a href="home.html">lifestyle</a></li>
                        <li class="yamm-fw"><a href="home.html">Sports</a></li>
                        <li class="yamm-fw" ><a href="home.html">travel</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="header-search">
                <div class="container">
                  <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                      <div class="navbar-search">
                        <form class="search-global">
                          <input type="text" placeholder="Type to search" autocomplete="off" name="s" value="" class="search-global__input"/>
                          <button class="search-global__btn"><i class="icon fa fa-search"></i></button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <button type="button" class="search-close close"><i class="fa fa-times"></i></button>
              </div>
            </nav>
            
    

    <script src="assets/js/main.js"></script>
    <script src="assets/js/separate-js/custom.js"></script>
    <!-- <script src="assets/plugins/3d-bold-navigation/main.js"></script> -->
    <script src="assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="assets/plugins/slider-pro/jquery.sliderPro.js"></script>
    <script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- <script src="assets/plugins/isotope/isotope.pkgd.min.js"></script> -->
    <script src="assets/plugins/prettyphoto/jquery.prettyPhoto.js"></script>
    <!-- <script src="assets/plugins/bootstrap-select/bootstrap-select.js"></script> -->
    <!-- <script src="assets/plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script> -->
    <script src="assets/plugins/doubletaptogo.js"></script>
    <script src="assets/plugins/waypoints.min.js"></script>
    <script src="assets/plugins/news-ticker/js/endlessRiver.js"></script>
    <script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script>
    <script src="assets/plugins/flexslider/jquery.flexslider.js"></script>
    <script src="assets/plugins/jarallax/jarallax.js"></script>
    <script src="assets/plugins/scrollreveal/scrollreveal.js"></script>
    <script src="assets/plugins/classie.js"></script>
    <script src="assets/plugins/switcher/js/dmss.js"></script>
  </body>
</html>
