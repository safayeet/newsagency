<div id="main-slider" data-slider-width="80%" data-slider-height="750" data-slider-hight="400px" data-orientation="vertical" class="main-slider main-slider_mod-a main-slider_dark text-center slider-pro">
              <div class="sp-slides">
                <!-- Slide 1-->
                <div class="sp-slide"><img src="assets/media/content/main-slider/1.jpg" alt="slider" class="sp-image"/>
                  <div data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="800" data-show-delay="400" data-hide-delay="400" class="sp-layer">
                    <h2 class="main-slider__title">Adieusm tempor incidunt dolore <br>sed magna enim</h2>
                  </div>
                  <div data-width="100%" data-show-transition="up" data-hide-transition="left" data-show-duration="800" data-show-delay="1700" data-hide-delay="400" class="sp-layer"><a href="blog-1.html" class="main-slider__link bg-3 btn btn-xs btn-effect">lifestyle</a></div>
                  <div data-width="100%" data-show-transition="right" data-hide-transition="left" data-show-duration="800" data-show-delay="1200" data-hide-delay="400" class="sp-layer">
                    <div class="main-slider-meta"><span class="main-slider-meta__item">february 30, 2016</span><span class="main-slider-meta__item"><i class="icon pe-7s-comment"></i>108</span></div>
                  </div>
                </div>
                <!-- Slide 2-->
                <div class="sp-slide"><img src="assets/media/content/main-slider/1.jpg" alt="slider" class="sp-image"/>
                  <div data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="800" data-show-delay="400" data-hide-delay="400" class="sp-layer">
                    <h2 class="main-slider__title">Adieusm tempor incidunt dolore <br>sed magna enim</h2>
                  </div>
                  <div data-width="100%" data-show-transition="up" data-hide-transition="left" data-show-duration="800" data-show-delay="1700" data-hide-delay="400" class="sp-layer"><a href="blog-1.html" class="main-slider__link bg-3 btn btn-xs btn-effect">lifestyle</a></div>
                  <div data-width="100%" data-show-transition="right" data-hide-transition="left" data-show-duration="800" data-show-delay="1200" data-hide-delay="400" class="sp-layer">
                    <div class="main-slider-meta"><span class="main-slider-meta__item">february 30, 2016</span><span class="main-slider-meta__item"><i class="icon pe-7s-comment"></i>108</span></div>
                  </div>
                </div>
                <!-- Slide 3-->
                <div class="sp-slide"><img src="assets/media/content/main-slider/1.jpg" alt="slider" class="sp-image"/>
                  <div data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="800" data-show-delay="400" data-hide-delay="400" class="sp-layer">
                    <h2 class="main-slider__title">Adieusm tempor incidunt dolore <br>sed magna enim</h2>
                  </div>
                  <div data-width="100%" data-show-transition="up" data-hide-transition="left" data-show-duration="800" data-show-delay="1700" data-hide-delay="400" class="sp-layer"><a href="blog-1.html" class="main-slider__link bg-3 btn btn-xs btn-effect">lifestyle</a></div>
                  <div data-width="100%" data-show-transition="right" data-hide-transition="left" data-show-duration="800" data-show-delay="1200" data-hide-delay="400" class="sp-layer">
                    <div class="main-slider-meta"><span class="main-slider-meta__item">february 30, 2016</span><span class="main-slider-meta__item"><i class="icon pe-7s-comment"></i>108</span></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <section class="section-type-a wow">
                  <div class="wrap-title-bg">
                    <h2 class="ui-title-bg">Latest from Cricket</h2>
                  </div>
                  <div data-min480="1" data-min768="2" data-min992="2" data-min1200="4" data-pagination="false" data-navigation="true" data-auto-play="4000" data-stop-on-hover="true" class="owl-carousel owl-theme enable-owl-carousel">
                    <article class="post post-1 clearfix">
                      <div class="entry-media"><a href="assets/media/content/post/285x300/1.jpg" class="prettyPhoto"><img src="assets/media/content/post/285x300/1.jpg" alt="Foto" class="img-responsive"/></a>
                        <h2 class="entry-title">Lorem ipsum dolor sit amet</h2>
                      </div><span class="label bg-1">politics</span>
                      <div class="entry-meta"><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">feb 30, 2016</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i>62</span></div>
                    </article>
                    <article class="post post-1 clearfix">
                      <div class="entry-media"><a href="assets/media/content/post/285x300/2.jpg" class="prettyPhoto"><img src="assets/media/content/post/285x300/2.jpg" alt="Foto" class="img-responsive"/></a>
                        <h2 class="entry-title">elit sed eiusmod tempor</h2>
                      </div><span class="label bg-2">technology</span>
                      <div class="entry-meta"><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">feb 30, 2016</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i>24</span></div>
                    </article>
                    <article class="post post-1 clearfix">
                      <div class="entry-media"><a href="assets/media/content/post/285x300/3.jpg" class="prettyPhoto"><img src="assets/media/content/post/285x300/3.jpg" alt="Foto" class="img-responsive"/></a>
                        <h2 class="entry-title">dolore magna aliqua Ut enim</h2>
                      </div><span class="label bg-5">world</span>
                      <div class="entry-meta"><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">feb 30, 2016</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i>47</span></div>
                    </article>
                    <article class="post post-1 clearfix">
                      <div class="entry-media"><a href="assets/media/content/post/285x300/4.jpg" class="prettyPhoto"><img src="assets/media/content/post/285x300/4.jpg" alt="Foto" class="img-responsive"/></a>
                        <h2 class="entry-title">ullamco laboris nisi aliquip</h2>
                      </div><span class="label bg-7">health</span>
                      <div class="entry-meta"><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">feb 30, 2016</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i>85</span></div>
                    </article>
                  </div>
                </section>
              </div>
            </div>
          </div>
          <div class="section-type-c wow">
            <div class="container">
              <div class="row">
                <div class="col-md-8">
                  <section class="section-area">
                    <h2 class="ui-title-block"><span class="ui-title-block__subtitle">Explore the Top</span><span class="text-uppercase">news makers</span></h2>
                    <ul class="nav nav-tabs">
                      <li class="tabs-label">Filter Content by</li>
                      <li class="active"><a href="#tab-1" data-toggle="tab">Latest</a></li>
                      <li><a href="#tab-2" data-toggle="tab">Enlish Football</a></li>
                      <li><a href="#tab-3" data-toggle="tab">Spanish Football</a></li>
                      <li><a href="#tab-4" data-toggle="tab">Italian football </a></li>
                    </ul>
                    <div class="tab-content">
                      <div id="tab-1" class="tab-pane fade in active">
                        <div class="row">
                          <div class="col-md-6">
                            <article class="post post-2 post-2_mod-d clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/360x300/1.jpg" class="prettyPhoto"><img src="assets/media/content/post/360x300/1.jpg" alt="Foto" class="img-responsive"/></a><span class="label bg-6">travel</span>
                              </div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title text-uppercase">Lorem ipsum dolor amt elit sed tempor incidunt</h2>
                                </div>
                                <div class="entry-meta"><span class="entry-meta__item">By<a href="news_details-1.html" class="entry-meta__link"> john sina</a></span><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">15 mins ago</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">23</a></span></div>
                                <div class="entry-content">
                                  <p>Lorem ipsum dolor amet consectetur adipisicing elit sed do eiusmod tempor incididunt labore et dolore magna aliqua enimad minim veniam quis nostruda exercitation ullamco laboris onsequat duis aute irue dolorin voluptate velit esse cillum ...</p>
                                </div>
                                <div class="entry-footer"><a href="news_details-1.html" class="btn-link">Continue Reading</a></div>
                              </div>
                            </article>
                          </div>
                          <div class="col-md-6">
                            <article class="post post-3 post-3_mod-a clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/1.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/1.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Incididunt ulabore dolore malnu alikua</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-4">entertainment</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">6</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 post-3_mod-a clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/2.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/2.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nsekuat duis</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-7">health</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">45</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 post-3_mod-a clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/3.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/3.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Perspiciatis unde omnist enatus error sit</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-5">world</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">81</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 post-3_mod-a clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/4.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/4.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Volupta tem acusa ntium dolore me laud</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-4">sports</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">48</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 post-3_mod-a clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/5.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/5.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nsewuat duis</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-12">technology</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">12</a></span></div>
                              </div>
                            </article>
                          </div>
                        </div>
                      </div>
                      <div id="tab-2" class="tab-pane fade">
                        <div class="row">
                          <div class="col-md-6">
                            <article class="post post-2 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/360x300/1.jpg" class="prettyPhoto"><img src="assets/media/content/post/360x300/1.jpg" alt="Foto" class="img-responsive"/></a><span class="label bg-6">travel</span>
                              </div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title text-uppercase">Lorem ipsum dolor amt elit sed tempor incidunt</h2>
                                </div>
                                <div class="entry-meta"><span class="entry-meta__item">By<a href="news_details-1.html" class="entry-meta__link"> john sina</a></span><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">15 mins ago</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">23</a></span></div>
                                <div class="entry-content">
                                  <p>Lorem ipsum dolor amet consectetur adipisicing elit sed do eiusmod tempor incididunt labore et dolore magna aliqua enimad minim veniam quis nostruda exercitation ullamco laboris onsequat duis aute irue dolorin voluptate velit esse cillum ...</p>
                                </div>
                                <div class="entry-footer"><a href="news_details-1.html" class="btn-link">Continue Reading</a></div>
                              </div>
                            </article>
                          </div>
                          <div class="col-md-6">
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/1.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/1.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Incididunt ulabore dolore malnu alikua</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-4">entertainment</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">6</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/2.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/2.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nsekuat duis</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-7">health</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">45</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/3.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/3.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Perspiciatis unde omnist enatus error sit</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-5">world</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">81</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/4.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/4.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Volupta tem acusa ntium dolore me laud</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-4">sports</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">48</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/5.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/5.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nsewuat duis</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-12">technology</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">12</a></span></div>
                              </div>
                            </article>
                          </div>
                        </div>
                      </div>
                      <div id="tab-3" class="tab-pane fade">
                        <div class="row">
                          <div class="col-md-6">
                            <article class="post post-2 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/360x300/1.jpg" class="prettyPhoto"><img src="assets/media/content/post/360x300/1.jpg" alt="Foto" class="img-responsive"/></a><span class="label bg-6">travel</span>
                              </div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title text-uppercase">Lorem ipsum dolor amt elit sed tempor incidunt</h2>
                                </div>
                                <div class="entry-meta"><span class="entry-meta__item">By<a href="news_details-1.html" class="entry-meta__link"> john sina</a></span><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">15 mins ago</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">23</a></span></div>
                                <div class="entry-content">
                                  <p>Lorem ipsum dolor amet consectetur adipisicing elit sed do eiusmod tempor incididunt labore et dolore magna aliqua enimad minim veniam quis nostruda exercitation ullamco laboris onsequat duis aute irue dolorin voluptate velit esse cillum ...</p>
                                </div>
                                <div class="entry-footer"><a href="news_details-1.html" class="btn-link">Continue Reading</a></div>
                              </div>
                            </article>
                          </div>
                          <div class="col-md-6">
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/1.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/1.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Incididunt ulabore dolore malnu alikua</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-4">entertainment</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">6</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/2.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/2.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nsekuat duis</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-7">health</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">45</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/3.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/3.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Perspiciatis unde omnist enatus error sit</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-5">world</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">81</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/4.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/4.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Volupta tem acusa ntium dolore me laud</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-4">sports</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">48</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/5.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/5.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nsewuat duis</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-12">technology</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">12</a></span></div>
                              </div>
                            </article>
                          </div>
                        </div>
                      </div>
                      <div id="tab-4" class="tab-pane fade">
                        <div class="row">
                          <div class="col-md-6">
                            <article class="post post-2 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/360x300/1.jpg" class="prettyPhoto"><img src="assets/media/content/post/360x300/1.jpg" alt="Foto" class="img-responsive"/></a><span class="label bg-6">travel</span>
                              </div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title text-uppercase">Lorem ipsum dolor amt elit sed tempor incidunt</h2>
                                </div>
                                <div class="entry-meta"><span class="entry-meta__item">By<a href="news_details-1.html" class="entry-meta__link"> john sina</a></span><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">15 mins ago</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">23</a></span></div>
                                <div class="entry-content">
                                  <p>Lorem ipsum dolor amet consectetur adipisicing elit sed do eiusmod tempor incididunt labore et dolore magna aliqua enimad minim veniam quis nostruda exercitation ullamco laboris onsequat duis aute irue dolorin voluptate velit esse cillum ...</p>
                                </div>
                                <div class="entry-footer"><a href="news_details-1.html" class="btn-link">Continue Reading</a></div>
                              </div>
                            </article>
                          </div>
                          <div class="col-md-6">
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/1.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/1.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Incididunt ulabore dolore malnu alikua</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-4">entertainment</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">6</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/2.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/2.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nsekuat duis</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-7">health</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">45</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/3.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/3.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Perspiciatis unde omnist enatus error sit</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-5">world</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">81</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/4.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/4.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Volupta tem acusa ntium dolore me laud</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-4">sports</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">48</a></span></div>
                              </div>
                            </article>
                            <article class="post post-3 clearfix">
                              <div class="entry-media"><a href="assets/media/content/post/100x100/5.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/5.jpg" alt="Foto" class="img-responsive"/></a></div>
                              <div class="entry-main">
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nsewuat duis</a></h2>
                                </div>
                                <div class="entry-meta"><span class="category color-12">technology</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">12</a></span></div>
                              </div>
                            </article>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
                <div class="col-md-4">
                  <aside class="sidebar">
                    <section class="widget">
                      <h2 class="widget-title ui-title-inner text-right">Live Fixture</h2>
                      <div class="decor-right"></div>
                      <div class="widget-content">
                        <iframe height="350" frameborder="5" style="vertical-align: bottom;"  src="http://tools.whatsthescore.com/tools.php?id=37615&iframe=tap-sport-tools-37615&tz-js=Asia/Dhaka"></iframe>
                      </div>
                    </section>
                    <div class="widget"><a href="home.html" class="banner"><img src="assets/media/content/banners/1.jpg" alt="banner" class="img-responsive center-block"></a></div>
                    <section class="widget">
                      <h2 class="widget-title ui-title-inner text-right">stay updated</h2>
                      <div class="decor-right"></div>
                      <div class="widget-content">
                        <p>Sign up for our newsletter to receive latest news as it happenes in your inbox.</p>
                        <form class="form-subscribe">
                          <div class="form-group has-feedback">
                            <input type="email" placeholder="your email address" class="form-control">
                            <button class="icon pe-7s-mail form-control-feedback"></button>
                          </div>
                        </form>
                      </div>
                    </section>
                  </aside>
                </div>
              </div>
            </div>
          </div>
          
		  
		  <div class="section-type-j wow">
            <div class="container">
              <div class="row">
                <div class="col-md-4">
                  <div class="title-category clearfix">
                    <h2 class="title-category__title ui-title-inner color-4">entertainment</h2><span class="title-category__category">fashion, movies, ...</span>
                  </div>
                  <div class="decor-right bg-4"></div>
                  <article class="post post-2 post-2_mod-c post-2__mrg-btn clearfix">
                    <div class="entry-media"><a href="assets/media/content/post/360x240/1.jpg" class="prettyPhoto"><img src="assets/media/content/post/360x240/1.jpg" alt="Foto" class="img-responsive"/></a>
                    </div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h2 class="entry-title text-uppercase">Incididunt labore et magna</h2>
                      </div>
                      <div class="entry-meta"><span class="entry-meta__item">By<a href="news_details-1.html" class="entry-meta__link"> john sina</a></span><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">15 mins ago</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">62</a></span></div>
                      <div class="entry-content">
                        <p>Lorem ipsum dolor amet consectetur adipisicing elit sed do eiusmod tempor incididunt labore et dolore magna aliqua enimad minim ...</p>
                      </div>
                      <div class="entry-footer"><a href="news_details-1.html" class="btn-link">Continue Reading</a></div>
                    </div>
                  </article>
                  <article class="post post-3 post-3_mod-c clearfix">
                    <div class="entry-media"><a href="assets/media/content/post/100x100/8.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/8.jpg" alt="Foto" class="img-responsive"/></a></div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nsewuat duis</a></h2>
                      </div>
                      <div class="entry-meta"><span class="category color-4">entertainment</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">54</a></span></div>
                    </div>
                  </article>
                  <article class="post post-3 post-3_mod-c clearfix">
                    <div class="entry-media"><a href="assets/media/content/post/100x100/10.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/10.jpg" alt="Foto" class="img-responsive"/></a></div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nsewuat duis</a></h2>
                      </div>
                      <div class="entry-meta"><span class="category color-4">entertainment</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">78</a></span></div>
                    </div>
                  </article>
                </div>
                <div class="col-md-4">
                  <div class="title-category clearfix">
                    <h2 class="title-category__title ui-title-inner color-5">world</h2><span class="title-category__category">Asia, europe, ...</span>
                  </div>
                  <div class="decor-right bg-5"></div>
                  <article class="post post-2 post-2_mod-c post-2__mrg-btn clearfix">
                    <div class="entry-media"><a href="assets/media/content/post/360x240/2.jpg" class="prettyPhoto"><img src="assets/media/content/post/360x240/2.jpg" alt="Foto" class="img-responsive"/></a>
                    </div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h2 class="entry-title text-uppercase">dolor amt elit sed tempor</h2>
                      </div>
                      <div class="entry-meta"><span class="entry-meta__item">By<a href="news_details-1.html" class="entry-meta__link"> john sina</a></span><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">15 mins ago</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">62</a></span></div>
                      <div class="entry-content">
                        <p>Lorem ipsum dolor amet consectetur adipisicing elit sed do eiusmod tempor incididunt labore et dolore magna aliqua enimad minim ...</p>
                      </div>
                      <div class="entry-footer"><a href="news_details-1.html" class="btn-link">Continue Reading</a></div>
                    </div>
                  </article>
                  <article class="post post-3 post-3_mod-c clearfix">
                    <div class="entry-media"><a href="assets/media/content/post/100x100/7.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/7.jpg" alt="Foto" class="img-responsive"/></a></div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h2 class="entry-title"><a href="news_details-1.html">Incididunt ulabore dolore malnu alikua</a></h2>
                      </div>
                      <div class="entry-meta"><span class="category color-5">world</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">51</a></span></div>
                    </div>
                  </article>
                  <article class="post post-3 post-3_mod-c clearfix">
                    <div class="entry-media"><a href="assets/media/content/post/100x100/9.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/9.jpg" alt="Foto" class="img-responsive"/></a></div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nasek euat duis</a></h2>
                      </div>
                      <div class="entry-meta"><span class="category color-5">world</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">14</a></span></div>
                    </div>
                  </article>
                </div>
                <div class="col-md-4">
                  <div class="title-category clearfix">
                    <h2 class="title-category__title ui-title-inner color-2">technology</h2><span class="title-category__category">smartphones, gadgets, ...</span>
                  </div>
                  <div class="decor-right bg-2"></div>
                  <article class="post post-2 post-2_mod-c post-2__mrg-btn clearfix">
                    <div class="entry-media"><a href="assets/media/content/post/360x240/3.jpg" class="prettyPhoto"><img src="assets/media/content/post/360x240/3.jpg" alt="Foto" class="img-responsive"/></a>
                    </div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h2 class="entry-title text-uppercase">Lorem ipsum dolor sit amet</h2>
                      </div>
                      <div class="entry-meta"><span class="entry-meta__item">By<a href="news_details-1.html" class="entry-meta__link"> john sina</a></span><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">15 mins ago</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">62</a></span></div>
                      <div class="entry-content">
                        <p>Lorem ipsum dolor amet consectetur adipisicing elit sed do eiusmod tempor incididunt labore et dolore magna aliqua enimad minim ...</p>
                      </div>
                      <div class="entry-footer"><a href="news_details-1.html" class="btn-link">Continue Reading</a></div>
                    </div>
                  </article>
                  <article class="post post-3 post-3_mod-c clearfix">
                    <div class="entry-media"><a href="assets/media/content/post/100x100/6.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/6.jpg" alt="Foto" class="img-responsive"/></a></div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h2 class="entry-title"><a href="news_details-1.html">Volupta tem acusa ntium dolore me laud</a></h2>
                      </div>
                      <div class="entry-meta"><span class="category color-2">technology</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">6</a></span></div>
                    </div>
                  </article>
                  <article class="post post-3 post-3_mod-c clearfix">
                    <div class="entry-media"><a href="assets/media/content/post/100x100/22.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/22.jpg" alt="Foto" class="img-responsive"/></a></div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h2 class="entry-title"><a href="news_details-1.html">Perspiciatis unde omnist enatus error sit</a></h2>
                      </div>
                      <div class="entry-meta"><span class="category color-2">technology</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">3</a></span></div>
                    </div>
                  </article>
                </div>
              </div>
            </div>
          </div>
		  
		  
		  
		  <div class="section-default">
            <div class="container">
              <div class="row">
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-6 wow">
                      <div class="title-category clearfix">
                        <h2 class="title-category__title ui-title-inner color-4">entertainment</h2><span class="title-category__category">fashion, movies, ...</span>
                      </div>
                      <div class="decor-right bg-4"></div>
                      <article class="post post-2 post-2_mod-c clearfix">
                        <div class="entry-media"><a href="assets/media/content/post/360x240/1.jpg" class="prettyPhoto"><img src="assets/media/content/post/360x240/1.jpg" alt="Foto" class="img-responsive"/></a>
                        </div>
                        <div class="entry-main">
                          <div class="entry-header">
                            <h2 class="entry-title text-uppercase">Incididunt labore et magna</h2>
                          </div>
                          <div class="entry-meta"><span class="entry-meta__item">By<a href="news_details-1.html" class="entry-meta__link"> john sina</a></span><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">15 mins ago</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">62</a></span></div>
                          <div class="entry-content">
                            <p>Lorem ipsum dolor amet consectetur adipisicing elit sed do eiusmod tempor incididunt labore et dolore magna aliqua enimad minim ...</p>
                          </div>
                          <div class="entry-footer"><a href="news_details-1.html" class="btn-link">Continue Reading</a></div>
                        </div>
                      </article>
                    </div>
                    <div class="col-md-6 wow">
                      <div class="title-category clearfix">
                        <h2 class="title-category__title ui-title-inner color-5">world news</h2><span class="title-category__category">Asia, europe, ...</span>
                      </div>
                      <div class="decor-right bg-5"></div>
                      <article class="post post-2 post-2_mod-c clearfix">
                        <div class="entry-media"><a href="assets/media/content/post/360x240/2.jpg" class="prettyPhoto"><img src="assets/media/content/post/360x240/2.jpg" alt="Foto" class="img-responsive"/></a>
                        </div>
                        <div class="entry-main">
                          <div class="entry-header">
                            <h2 class="entry-title text-uppercase">dolor amt elit sed tempor</h2>
                          </div>
                          <div class="entry-meta"><span class="entry-meta__item">By<a href="news_details-1.html" class="entry-meta__link"> john sina</a></span><span class="entry-meta__item"><a href="news_details-1.html" class="entry-meta__link">15 mins ago</a></span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">62</a></span></div>
                          <div class="entry-content">
                            <p>Lorem ipsum dolor amet consectetur adipisicing elit sed do eiusmod tempor incididunt labore et dolore magna aliqua enimad minim ...</p>
                          </div>
                          <div class="entry-footer"><a href="news_details-1.html" class="btn-link">Continue Reading</a></div>
                        </div>
                      </article>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="section-type-d wow"><a href="home.html" class="banner"><img src="assets/media/content/banners/2.jpg" alt="banner" class="img-responsive center-block"></a></div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 wow">
                      <article class="post post-3 post-3_mod-c clearfix">
                        <div class="entry-media"><a href="assets/media/content/post/100x100/6.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/6.jpg" alt="Foto" class="img-responsive"/></a></div>
                        <div class="entry-main">
                          <div class="entry-header">
                            <h2 class="entry-title"><a href="news_details-1.html">Volupta tem acusa ntium dolore me laud</a></h2>
                          </div>
                          <div class="entry-meta"><span class="category color-4">entertainment</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">6</a></span></div>
                        </div>
                      </article>
                      <article class="post post-3 post-3_mod-c clearfix">
                        <div class="entry-media"><a href="assets/media/content/post/100x100/8.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/8.jpg" alt="Foto" class="img-responsive"/></a></div>
                        <div class="entry-main">
                          <div class="entry-header">
                            <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nsewuat duis</a></h2>
                          </div>
                          <div class="entry-meta"><span class="category color-4">entertainment</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">54</a></span></div>
                        </div>
                      </article>
                      <article class="post post-3 post-3_mod-c clearfix">
                        <div class="entry-media"><a href="assets/media/content/post/100x100/10.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/10.jpg" alt="Foto" class="img-responsive"/></a></div>
                        <div class="entry-main">
                          <div class="entry-header">
                            <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nsewuat duis</a></h2>
                          </div>
                          <div class="entry-meta"><span class="category color-4">entertainment</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">78</a></span></div>
                        </div>
                      </article>
                    </div>
                    <div class="col-md-6 wow">
                      <article class="post post-3 post-3_mod-c clearfix">
                        <div class="entry-media"><a href="assets/media/content/post/100x100/7.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/7.jpg" alt="Foto" class="img-responsive"/></a></div>
                        <div class="entry-main">
                          <div class="entry-header">
                            <h2 class="entry-title"><a href="news_details-1.html">Incididunt ulabore dolore malnu alikua</a></h2>
                          </div>
                          <div class="entry-meta"><span class="category color-5">world</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">51</a></span></div>
                        </div>
                      </article>
                      <article class="post post-3 post-3_mod-c clearfix">
                        <div class="entry-media"><a href="assets/media/content/post/100x100/9.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/9.jpg" alt="Foto" class="img-responsive"/></a></div>
                        <div class="entry-main">
                          <div class="entry-header">
                            <h2 class="entry-title"><a href="news_details-1.html">Aliquip exea comod rure nasek euat duis</a></h2>
                          </div>
                          <div class="entry-meta"><span class="category color-5">world</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">14</a></span></div>
                        </div>
                      </article>
                      <article class="post post-3 post-3_mod-c clearfix">
                        <div class="entry-media"><a href="assets/media/content/post/100x100/11.jpg" class="prettyPhoto"><img src="assets/media/content/post/100x100/11.jpg" alt="Foto" class="img-responsive"/></a></div>
                        <div class="entry-main">
                          <div class="entry-header">
                            <h2 class="entry-title"><a href="news_details-1.html">Perspiciatis unde omnist enatus error sit</a></h2>
                          </div>
                          <div class="entry-meta"><span class="category color-5">world</span><span class="entry-meta__item"><i class="icon pe-7s-comment"></i><a href="news_details-1.html" class="entry-meta__link">3</a></span></div>
                        </div>
                      </article>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <aside class="sidebar">
                    <section class="widget wow">
                      <h2 class="widget-title ui-title-inner text-right">categories</h2>
                      <div class="decor-right"></div>
                      <div class="widget-content">
                        <ul class="list list-mark-1 list-mark-1_mod-a">
                          <li><a href="category.html">fashion & lifestyle</a></li>
                          <li><a href="category.html">World politics</a></li>
                          <li><a href="category.html">entertainment News</a></li>
                          <li><a href="category.html">music & videos</a></li>
                          <li><a href="category.html">fun & funny moments</a></li>
                        </ul>
                      </div>
                    </section>
                    <section class="widget wow">
                      <h2 class="widget-title ui-title-inner text-right">Latest videos</h2>
                      <div class="decor-right"></div>
                      <div class="widget-content">
                        <div id="accordion" class="panel-group acc-type-a acc-type-a_mod-a">
                          <div class="panel panel-default">
                            <div id="vi-ac_1" class="panel-collapse collapse in">
                              <div class="panel-body">
                                <iframe width="100%" height="240" src="https://www.youtube.com/embed/_BvUJNWvysg?rel=0&amp;amp;showinfo=0;theme=light;iv_load_policy=3;modestbranding=1;autohide=1" frameborder="0" allowfullscreen></iframe>
                              </div>
                            </div>
                            <div class="panel-heading">
                              <div class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#vi-ac_1"><span class="acc-type-a__title"><i class="icon fa fa-play"></i>Journey into west sea</span><span class="acc-type-a__author">BY anderson</span></a></div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div id="vi-ac_2" class="panel-collapse collapse">
                              <div class="panel-body">
                                <iframe width="100%" height="240" src="https://www.youtube.com/embed/_BvUJNWvysg?rel=0&amp;amp;showinfo=0;theme=light;iv_load_policy=3;modestbranding=1;autohide=1" frameborder="0" allowfullscreen></iframe>
                              </div>
                            </div>
                            <div class="panel-heading">
                              <div class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#vi-ac_2"><span class="acc-type-a__title"><i class="icon fa fa-play"></i>Aliquic exea comod rureduis</span><span class="acc-type-a__author">BY sofia</span></a></div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div id="vi-ac_3" class="panel-collapse collapse">
                              <div class="panel-body">
                                <iframe width="100%" height="240" src="https://www.youtube.com/embed/_BvUJNWvysg?rel=0&amp;amp;showinfo=0;theme=light;iv_load_policy=3;modestbranding=1;autohide=1" frameborder="0" allowfullscreen></iframe>
                              </div>
                            </div>
                            <div class="panel-heading">
                              <div class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#vi-ac_3"><span class="acc-type-a__title"><i class="icon fa fa-play"></i>Dolore magna aliqua ut enim nim</span><span class="acc-type-a__author">BY clarkson</span></a></div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div id="vi-ac_4" class="panel-collapse collapse">
                              <div class="panel-body">
                                <iframe width="100%" height="240" src="https://www.youtube.com/embed/_BvUJNWvysg?rel=0&amp;amp;showinfo=0;theme=light;iv_load_policy=3;modestbranding=1;autohide=1" frameborder="0" allowfullscreen></iframe>
                              </div>
                            </div>
                            <div class="panel-heading">
                              <div class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#vi-ac_4"><span class="acc-type-a__title"><i class="icon fa fa-play"></i>Slamco laboris nisi ute aliquip</span><span class="acc-type-a__author">BY zedan</span></a></div>
                            </div>
                          </div>
                        </div>
                        <div class="text-right"><a href="home.html" class="acc-type-a__link btn-link">all videos</a></div>
                      </div>
                    </section>
                  </aside>
                </div>
              </div>
            </div>
          </div>
         
          <footer class="footer">
            <div class="container">
              <div class="row">
                <div class="col-md-4">
                  <div class="footer__first-section"><a href="home.html" class="footer__logo"><img src="assets/media/general/logo_mod-b.png" alt="logo" class="img-responsive"></a>
                    <div class="footer__info">Lorem ipsum dolor amet consecteu adipisicing sed do eiusmod tempor incididunt labore dolore magna aliqua enimad minim tempor incididunt labore et dolore magna aliqua.</div>
                    <div class="decor-right decor-right_sm"></div>
                    
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="clearfix">
                    <div class="footer-wrap-section">
                      <section class="footer-section">
                        <h3 class="footer__title ui-title-inner">categories</h3>
                        <div class="decor-right decor-right_sm bg-7"></div>
                        
                      </section>
                      <section class="footer-section">
                        <h3 class="footer__title ui-title-inner">our partners</h3>
                        <div class="decor-right decor-right_sm bg-13"></div>
                        
                      </section>
                      <section class="footer-section">
                        <h3 class="footer__title ui-title-inner">get Connect</h3>
                        <div class="decor-right decor-right_sm bg-3"></div>
                        
                      </section>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
			
            <div class="footer-bottom">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="footer-bottom__link bg-primary">
                      <div class="footer-bottom__wrap-link">we are social<i class="icon fa fa-caret-right"></i></div>
                    </div>
                    <div class="footer-bottom__inner">
                                <ul class="social-links list-inline">
                                  <li><a href="twitter.com" class="social-links__item_link"><i class="icon fa fa-twitter"></i></a></li>
                                  <li><a href="facebook.com" class="social-links__item_link"><i class="icon fa fa-facebook"></i></a></li>
                                  <li><a href="instagram.com" class="social-links__item_link"><i class="icon fa fa-instagram"></i></a></li>
                                  <li><a href="linkedin.com" class="social-links__item_link"><i class="icon fa fa-linkedin"></i></a></li>
                                  <li><a href="pinterest.com" class="social-links__item_link"><i class="icon fa fa-pinterest-p"></i></a></li>
                                  <li><a href="youtube.com" class="social-links__item_link"><i class="icon fa fa-youtube-play"></i></a></li>
                                  <li><a href="rss.com" class="social-links__item_link"><i class="icon fa fa-rss"></i></a></li>
                                </ul>
                    </div>
                    <div class="copyright">� 2016<a href="home.html"> NEWS AGENCY.</a> All rights reserved.</div>
                  </div>
                </div>
              </div>
            </div>
          </footer>
          <!-- end wrap-content-->
        </div>
      </div>
    </div>