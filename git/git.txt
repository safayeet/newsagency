Git global setup

git config --global user.name "safayeet"
git config --global user.email "safayeet@gmail.com"

Create a new repository

git clone https://safayeet@gitlab.com/safayeet/newsagency.git
cd newsagency
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://safayeet@gitlab.com/safayeet/newsagency.git
git add .
git commit
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://safayeet@gitlab.com/safayeet/newsagency.git
git push -u origin --all
git push -u origin --tags